﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

using Informer.Gui.Selemente;
using Informer.Gui.Selemente.Liste;
using Informer.Utils;
using Informer.Gui.Uc;
using Informer.Model;

using UcListbox = Informer.Gui.Selemente.Liste.UcListbox;
using UcTodoListe = Informer.Gui.Selemente.Todo.UcTodoListe;
using WinItemsEdit = Informer.Gui.WindowKlein.WinItemsEdit;

namespace Informer.Gui
{

    /// <summary>
    /// Interaktionslogik für InformerView.xaml
    /// </summary>
    public partial class InformerView : UserControl
    {
        #region Attribute ---------------------------------------------------------------

        private readonly ExecEditor ccExecEditor;

        #endregion


        #region ctor --------------------------------------------------------------------

        public InformerView()
        {
            InitializeComponent( );
            //lst_dirliste.ItemsSource = loadListBoxData();
            //lst_dirliste.DataContext = loadListBoxData();
            //ccExecEditor = new ExecEditor( this );
            ccExecEditor = new ExecEditor( );
            //this.DataContext = ccFirstlineData;

            // -- Daten aus dem .dis-File laden.
            G.ccDataLoadInst = new DataLoad( );
            G.ccDataLoadInst.load( );

            if ( G.ccDataLoadInst.ccStatus == DataLoad.EStatus.Init )
            {
                if ( MessageBox.Show( "Neuer Disfile. Soll er initialisiert werden?", "Init Disfile",
                                     MessageBoxButton.YesNo,
                                     MessageBoxImage.Question, MessageBoxResult.Yes ) == MessageBoxResult.Yes )
                {
                    G.ccDataLoadInst.initDisFile( );
                    MessageBox.Show(
                        "Der Disfile ist initialisiert worden. Das Programm wird jetzt beendet. Starten Sie das Programm durch doppelklick auf den Disfile erneut.",
                        "Init Disfile" );
                    Environment.Exit( 0 );
                }

                Environment.Exit( 0 );
            }

            if ( G.ccDataLoadInst.ccStatus != DataLoad.EStatus.Load )
            {
                MessageBox.Show(
                    "Der Disfile ist nicht geladen worden. Das Programm wird jetzt beendet.",
                    "Load Disfile" );
                Environment.Exit( 1 );
            }

            loadAndShow( );

            //var a1 = new Utils.ListHeader( ListHeader.listHeaderEnum.csv, "$F:0:2:55:Todos:Autosort" );
            //var a2 = a1.ToString( );
            //var b1 = new Utils.ListHeader( ListHeader.listHeaderEnum.csvold, "$F:2:Todos Autosort" );
            //var b2 = b1.ToString( );
            //var d1 = new Utils.ListHeader( ListHeader.listHeaderEnum.json, "$list={ \"typ\": \"F\", \"id\": 0, \"order\": 2, \"orderold\": 73, \"group\": \"Todos\", \"text\": \"Autosort\"}" );
            //var d2 = d1.ToString( );


        }

        #endregion

        public void loadAndShow()
        {
            mainGridLeft.Children.Clear( );
            mainGridLeft.Children.Add( loadListBoxData( ).leftPanel );
            mainGridRight.Children.Add( loadListBoxData( ).rightPanel );
        }


        /// <summary>
        /// Master Einsprung
        /// 
        /// Liest die Daten die in der ListView angezeigt werden soll.
        /// </summary>
        /// <returns></returns>
        private (StackPanel leftPanel, StackPanel rightPanel) loadListBoxData()
        {
            // -- die grafischen elemente erstellen
            StackPanel lStpLeft = new StackPanel( );
            StackPanel lStpRight = new StackPanel( );

            foreach ( var lSelement in G.ccMainSelementeData )
            {

                if ( lSelement.Item.Steuertype == Model.ESteuerTyps.String.ToString( ) )
                {
                    lStpLeft.Children.Add( createTextbox( lSelement ) );
                }
                if ( lSelement.Item.Steuertype == Model.ESteuerTyps.Liste.ToString( ) )
                {
                    lStpLeft.Children.Add( createListbox( lSelement ) );
                }
                if ( lSelement.Item.Steuertype == Model.ESteuerTyps.Todo.ToString( ) )
                {
                    lStpRight.Children.Add( createTodoListe( lSelement ) );
                }

            }
            return (lStpLeft, lStpRight);
        }


        private ucTextbox createTextbox( Model.Selemente xSelemente )
        {
            var sas = xSelemente.Item.Sdata.StypeString;  //Sdata2.Ptext.String;
            var st = new ucTextbox
            {
                DataContext = xSelemente,
                //lblLabel = { Content = xSelemente.Item.Name },
                //txtTextbox = { DataContext = xSelemente,  Height = 21 * xSelemente.Item.Attribute.Len }
                txtTextbox = { Height = 21 * xSelemente.Item.Attribute.Len }
            };

            return st;
        }



        private UcListbox createListbox( Model.Selemente xSelemente )
        {

            List<ListboxData> convertTextlineToListboxdata( string xText )
            {
                var li = xText.Split( '\r' );

                List<ListboxData> lili2 = new List<ListboxData>( );
                foreach ( var lS in li )
                {
                    var iu = lS.Split( new[] { ';' }, 2, StringSplitOptions.RemoveEmptyEntries );
                    var da = new ListboxData
                    {
                        ccName = iu[0].Trim( ),
                        ccZusatz = iu.Length > 1 ? iu[1].Trim( new[] { ')', ' ', '\r' } ) : ""
                    };

                    lili2.Add( da );
                }

                return lili2;
            }

            List<ListboxData> lili = new List<ListboxData>( );
            lili = convertTextlineToListboxdata( xSelemente.Item.Sdata.StypeListe );

            var iool = new UcListbox( )
            {
                lbl_label = { Content = xSelemente.Item.Label },
                lbox_listbox = { DataContext = xSelemente, ItemsSource = lili, Height = 21 * xSelemente.Item.Attribute.Len }
            };

            return iool;
        }


        private string convertListboxdataToTextline( List<ListboxData> xValue )
        {
            return null;
        }

        /// <summary>
        /// <code>"wpf listbox master-detail binding"</code>
        /// <see cref="https://code.msdn.microsoft.com/windowsdesktop/CSWPFMasterDetailBinding-c78566ae"/>
        /// <see cref="https://stackoverflow.com/questions/42073432/wpf-ef-databinding-itemscontrol-inside-listbox-master-detail"/>
        /// </summary>
        /// <param name="xSelemente"></param>
        /// <returns></returns>
        private UcTodoListe createTodoListe( Model.Selemente xSelemente )
        {
            return new UcTodoListe
            {
                lblLabel = { Content = xSelemente.Item.Label },
                lboxListbox = { ItemsSource = xSelemente.Item.Sdata.StypeTodo },
                txtTextbox = { DataContext = xSelemente.Item.Sdata.StypeTodo },
                grd_gridbox = { }
            };
        }


        #region Events ------------------------------------------------------------------

        private void UserControl_Loaded( object sender, RoutedEventArgs e )
        {
            // -- setzt den Gridsplitter richtig
            var sss = Properties.Settings.Default.ccGridBoxMitte;
            igrid.ColumnDefinitions[0].Width = new GridLength( sss );
        }


        /// <summary>
        /// verliert den Fokus.
        /// Hier wird gespeichert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_LostFocus( object sender, RoutedEventArgs e )
        {
            Properties.Settings.Default.ccGridBoxMitte = igrid.ColumnDefinitions[0].ActualWidth;
            Properties.Settings.Default.Save( );
            //ccDataLoadInst.save( );   // Temporär ausgeschaltet
        }


        /// <summary>
        /// Button - Editor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReadmeeditor_Click( object sender, RoutedEventArgs e )
        {
            ccExecEditor.execEditor( );
        }


        private void btnSave_Click( object sender, RoutedEventArgs e )
        {
            G.ccDataLoadInst.save( );
        }


        private void btnRefresh_Click( object sender, RoutedEventArgs e )
        {
            G.ccDataLoadInst.refresh( );
            loadAndShow( );

        }


        private void mitEditItem_Click( object sender, RoutedEventArgs e )
        {
            // https://social.msdn.microsoft.com/Forums/de-DE/fa107b3d-d197-4b37-8872-3434f5bbcd12/wpf-combobox-mehrere-spaltenfelder-anzeigen-aber-nur-einen-wert-bernehmen?forum=wpfde
            WinItemsEdit lWin = new WinItemsEdit( );
            lWin.Owner = Application.Current.MainWindow;
            lWin.txtHead.Text = "Info";
            lWin.txtText.Text = @"
        object[] a1 = {
            la.Item.Id,
            la.Item.Name,
            la.Item.Steuertype,
            la.Item.Vpos,
            la.Item.Len,
            la.Item.Hpos
        };";

            lWin.brdTop.DataContext = G.ccMainSelementeData;
            lWin.cmbSteuertype.ItemsSource = Enum.GetValues( typeof( Libs.lbb.ESteuerTypen ) ).Cast<Libs.lbb.ESteuerTypen>( );

            lWin.ShowDialog( );

            // -- so könnten die daten einzeln gespeichert werden
            //     -- nicht vollständig --
            //var i = ccS( lWin );

            //G.ccMainSelementeData[i].Item.Name = lWin.txtName.Text;
            //G.ccMainSelementeData[i].Item.Id = lWin.txtId.Text;
            //G.ccMainSelementeData[i].Item.Attribute.Len = long.Parse( lWin.txtLen.Text );
            //G.ccMainSelementeData[i].Item.Attribute.Vpos = long.Parse( lWin.txtOrder.Text );
            //G.ccMainSelementeData[i].Item.Attribute.Hpos = long.Parse( lWin.txtPos.Text );

            ////G.ccMainSelementeData[i].Item.Steuertype   = lWin.cmbSteuertype.SelectedItem;

            //object[] a1 = { lWin.cmbSteuertype.SelectedItem,
            //                lWin.txtName.Text, lWin.txtLen.Text, lWin.txtHead.Text, lWin.txtId.Text,
            //                lWin.txtOrder.Text, lWin.txtPos.Text, lWin.txtText.Text };



            // -- Speichern und neu Anzeigen
            G.ccDataLoadInst.saveAndRefresh( );
            loadAndShow( );
        }


        private void mitAddItem_Click( object sender, RoutedEventArgs e )
        {
            var lItem = new Model.Item( );

            lItem.Pid = Libs.getPid( xName : "Neu" );
            lItem.Label = "Neu";
            lItem.Steuertype = Model.ESteuerTyps.String.ToString( );

            lItem.Attribute = new Model.Attribute { Hpos = 2, Vpos = 99, Len = 1 };
            lItem.Sdata = new Model.Sdata { StypeTodo = null, StypeString = "--neu--", StypeListe = null };

            var js = new Model.Selemente( );
            js.Item = lItem;


            G.ccMainSelementeData.Add( js );

            // -- Speichern und neu Anzeigen
            G.ccDataLoadInst.saveAndRefresh( );
            loadAndShow( );
        }


        private void mitDeleteItem_Click( object sender, RoutedEventArgs e )
        {
            WinItemsEdit lWin = new WinItemsEdit
            {
                Owner = Application.Current.MainWindow,
                txtHead = { Text = "Steuerelement Löschen" },
                txtText = { Text = "Löscht ein Steuerelement. Wähle das zu löschende Steuerelement aus und klicke "+
                                   "Ok. Nach einer Sicherheitsabfrage wird das Element gelöscht."},
                brdTop = { DataContext = G.ccMainSelementeData },
                cmbSteuertype = { ItemsSource = Enum.GetValues( typeof( Libs.lbb.ESteuerTypen ) ).Cast<Libs.lbb.ESteuerTypen>( ) }
            };

            lWin.ShowDialog( );

            var i = ccS( lWin );

            G.ccMainSelementeData.RemoveAt( i );

            // -- Speichern und neu Anzeigen
            G.ccDataLoadInst.saveAndRefresh( );
            loadAndShow( );
        }


        private static short ccS( WinItemsEdit lWin )
        {
            Model.Selemente ss1 = (Model.Selemente)lWin.brdLeft.DataContext;

            Int16 i;
            for ( i = 0; i <= G.ccMainSelementeData.Count; i++ )
            {
                if ( G.ccMainSelementeData[i].Item.Pid.Id == ss1.Item.Pid.Id )
                {
                    break;
                }
            }
            return i;
        }


        // todo: mitAction_Click: Muss noch implementiert werden
        private void mitAction_Click( object sender, RoutedEventArgs e )
        {

        }

        #endregion

    }
}


