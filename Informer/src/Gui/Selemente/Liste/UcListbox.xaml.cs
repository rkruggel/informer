﻿using System;
using System.Windows;
using System.Windows.Controls;

using Informer.Utils;

namespace Informer.Gui.Selemente.Liste
{

    /// <summary>
    /// Interaktionslogik für UcListbox.xaml
    /// </summary>
    public partial class UcListbox : UserControl
    {
        public UcListbox()
        {
            InitializeComponent( );
        }

        private void lbox_listbox_SelectionChanged( object sender, SelectionChangedEventArgs e )
        {
            ListBox tx = (ListBox)sender;
            var selit = tx.SelectedItem;
        }

        private void mitEdit_Click( object sender, RoutedEventArgs e )
        {
            var lWin = new WinListEdit( );
            lWin.Owner = Application.Current.MainWindow;

            lWin.Owner = Application.Current.MainWindow;
            lWin.txtInfoHead.Text = "Listen";
            lWin.txtInfoText.Text = @"Definieren und ändern einer Liste. Die Liste wird als freier Text definiert. Die Rows werden durch ein 'Return' getrennt und die Columns durch ein ';' getrennt werden.. 

Bspl.
  Roland;Kruggel; 58
  Thomas;Berger; 42
";

            //lWin.DataContext = G.ccDataLoadInst.informerMainData.Selemente;
            lWin.DataContext = lbox_listbox.DataContext;

            //var uu = G.ccDataLoadInst.informerMainData.Selemente[1];
            //var sf = uu[1].Item.Sdata.StypeListe;

            //ListboxData aa1 = (ListboxData)this.lbox_listbox.SelectedItems[0];
            //lo.txt_p1.Text = aa1.ccName;
            //lo.txt_p2.Text = aa1.ccZusatz;

            lWin.ShowDialog( );

            //aa1.ccName = lo.txt_p1.Text;
            //aa1.ccZusatz = lo.txt_p2.Text;

            //lbox_listbox.Items.Add( "jjjjj" );

        }

        private void mitAdd_Click( object sender, RoutedEventArgs e )
        {

        }

        private void mitDelete_Click( object sender, RoutedEventArgs e )
        {
            int selectedIndex = lbox_listbox.SelectedIndex;
            try
            {
                lbox_listbox.Items.RemoveAt( selectedIndex );
            }
            catch ( Exception ex )
            {
                // Der Vorgang ist während der Verwendung von "ItemsSource" ungültig. 
                //  Verwenden Sie stattdessen "ItemsControl.ItemsSource", um auf Elemente zuzugreifen und diese zu ändern.
                Libs.message( "efte", xText: "Kein Element ist selektiert", xEx: ex, xIcon: Libs.MbImage.err );
            }

        }

        private void mitAction_Click( object sender, RoutedEventArgs e )
        {

        }

    }

    public class ListboxData
    {
        public string ccName { get; set; }
        public string ccZusatz { get; set; }
    }
}
