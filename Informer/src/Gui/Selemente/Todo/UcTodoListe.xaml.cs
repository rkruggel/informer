﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using Informer.Utils;


namespace Informer.Gui.Selemente.Todo
{
    /// <summary>
    /// Interaktionslogik für ucTodoListe.xaml
    /// </summary>
    public partial class UcTodoListe : UserControl
    {
        private CollectionView ccCollectionView;


        #region .ctor

        public UcTodoListe()
        {
            InitializeComponent( );
        }

        #endregion


        #region Handle

        private void UserControl_Loaded( object sender, RoutedEventArgs e )
        {
            // -- setzt den Gridsplitter richtig
            var sss = Properties.Settings.Default.ccGridBoxText;
            grd_gridbox.ColumnDefinitions[0].Width = new GridLength( sss );

            // -- Die Filter in der ListView setzen
            ccCollectionView = (CollectionView)CollectionViewSource.GetDefaultView( lboxListbox.ItemsSource );
            GroupFilter gf = new GroupFilter( );
            gf.AddFilter( TodoFilterTyp );
            gf.AddFilter( TodoFilterGroup );
            gf.AddFilter( TodoFilterText );

            ccCollectionView.Filter = gf.Filter;
        }

        private void UserControl_LostFocus( object sender, RoutedEventArgs e )
        {
            Properties.Settings.Default.ccGridBoxText = grd_gridbox.ColumnDefinitions[0].ActualWidth;
            Properties.Settings.Default.Save( );
        }


        private void mitEdit_Click( object sender, RoutedEventArgs e )
        {
            // -- Daten für das neue KleinWindow
            WinTodoEditData lWinTodoEditData = null;

            // -- Instanz vom neuen KleinWindow
            WinTodoEdit efl = new WinTodoEdit
            {
                Owner = Application.Current.MainWindow,
                ccTitle = "--Win Todo Edit--",
                ccHeader = "Edit",
                ccText = "Editiert die Todo Liste. Es können Typ, Order, Group und Text geändert werden. " +
                         "Die Möglichkeit der Änderung ist weitgehen wahlfrei."
            };

            // -- Den selectierten Datensatz aus der CollectionView
            Model.StypeTodo lCurrentItem = (Model.StypeTodo)ccCollectionView.CurrentItem;

            // -- dem KleinWindow die Daten zuweisen.
            lWinTodoEditData = SetWindowKleinData( lCurrentItem );

            // -- den Datacontext in dem kleinWindow und die initialen Daten setzen
            efl.stackPanel.DataContext = lWinTodoEditData;

            // -- Anzeigen
            efl.ShowDialog( );

            if ( efl.doSave )
            {
                lCurrentItem.Listekopf.Typ = lWinTodoEditData.ccTyp.Substring( 0, 1 );
                lCurrentItem.Listekopf.Order = lWinTodoEditData.ccOrder;
                lCurrentItem.Listekopf.Group = lWinTodoEditData.ccGroup;
                lCurrentItem.Listekopf.Text = lWinTodoEditData.ccText;

                G.ccDataLoadInst.save( );
            }
        }


        private void mitAdd_Click( object sender, RoutedEventArgs e )
        {
            // -- Daten für das neue KleinWindow
            WinTodoEditData lWinTodoEditData = null;

            // -- Instanz vom neuen KleinWindow
            WinTodoEdit efl = new WinTodoEdit
            {
                Owner = Application.Current.MainWindow,
                ccTitle = "--Win Todo Add--",
                ccHeader = "Add",
                ccText = "Hinzufügen eines neuen Listenelementes"
            };

            // -- Den selectierten Datensatz aus der CollectionView
            Model.StypeTodo lCurrentItem = (Model.StypeTodo)ccCollectionView.CurrentItem;

            // -- dem KleinWindow die Daten zuweisen.
            lWinTodoEditData = SetWindowKleinData( lCurrentItem );

            // -- den Datacontext in dem kleinWindow und die initialen Daten setzen
            efl.stackPanel.DataContext = lWinTodoEditData;

            // -- Anzeigen
            efl.ShowDialog( );

            if ( efl.doSave )
            {
                var lLiKopf = new Model.Listekopf
                {
                    Pid = Libs.getPid( ),
                    Typ = lWinTodoEditData.ccTyp.Substring( 0, 1 ),
                    Order = lWinTodoEditData.ccOrder,
                    Group = lWinTodoEditData.ccGroup,
                    Text = lWinTodoEditData.ccText
                };

                var lLiData = new Model.Listedata
                {
                    Text = lWinTodoEditData.ccText
                };

                var lLi = new Model.StypeTodo
                {
                    Listedata = lLiData,
                    Listekopf = lLiKopf
                };

                ((List<Model.StypeTodo>)lboxListbox.ItemsSource).Add( lLi );


                G.ccDataLoadInst.saveAndRefresh( );
                G.ccInformerView.loadAndShow( );
            }

        }


        private void mitDelete_Click( object sender, RoutedEventArgs e )
        {
            // -- Daten für das neue KleinWindow
            WinTodoEditData lWinTodoEditData = null;

            // -- Instanz vom neuen KleinWindow
            WinTodoEdit efl = new WinTodoEdit
            {
                Owner = Application.Current.MainWindow,
                ccTitle = "--Win Todo Delete--",
                ccHeader = "Delete",
                ccText = "Löschen des selectierten Listenelements",
                cmb_typ = { IsEnabled = false },
                txtOrdernr = { IsEnabled = false },
                txtGroup = { IsEnabled = false },
                txt_text = { IsEnabled = false }
            };

            // -- Den selectierten Datensatz aus der CollectionView
            Model.StypeTodo lCurrentItem = (Model.StypeTodo)ccCollectionView.CurrentItem;

            // -- dem KleinWindow die Daten zuweisen.
            lWinTodoEditData = SetWindowKleinData( lCurrentItem );

            // -- den Datacontext in dem kleinWindow und die initialen Daten setzen
            efl.stackPanel.DataContext = lWinTodoEditData;

            // -- Anzeigen
            efl.ShowDialog( );

            if ( efl.doSave )
            {
                ((List<Model.StypeTodo>)lboxListbox.ItemsSource).Remove( lCurrentItem );

                G.ccDataLoadInst.saveAndRefresh( );
                G.ccInformerView.loadAndShow( );
            }

        }


        private void mitConfig_Click( object xSender, RoutedEventArgs xE )
        {
            // -- Daten für das neue KleinWindow
            WinTodoConfigData lWinTodoConfigData = null;

            // -- Instanz vom neuen KleinWindow
            WinTodoConfig efl = new WinTodoConfig
            {
                Owner = Application.Current.MainWindow,
                ccTitle = "--Win Todo Config--",
                ccHeader = "Config",
                ccText = "Konfigurieren der Todoelemente"
            };

            // -- Den selectierten Datensatz aus der CollectionView
            Model.StypeTodo lCurrentItem = (Model.StypeTodo)ccCollectionView.CurrentItem;

            // -- dem KleinWindow die Daten zuweisen.
            //lWinTodoConfigData = SetWindowKleinData( lCurrentItem );

            // -- den Datacontext in dem kleinWindow und die initialen Daten setzen
            //efl.stackPanel.DataContext = lWinTodoConfigData;

            // -- Anzeigen
            efl.ShowDialog( );

            if ( efl.doSave )
            {
                //var lLiKopf = new Model.Listekopf
                //{
                //    Pid = Libs.getPid( ),
                //    Typ = lWinTodoEditData.ccTyp.Substring( 0, 1 ),
                //    Order = lWinTodoEditData.ccOrder,
                //    Group = lWinTodoEditData.ccGroup,
                //    Text = lWinTodoEditData.ccText
                //};

                //var lLiData = new Model.Listedata
                //{
                //    Text = lWinTodoEditData.ccText
                //};

                //var lLi = new Model.StypeTodo
                //{
                //    Listedata = lLiData,
                //    Listekopf = lLiKopf
                //};

                //((List<Model.StypeTodo>)lboxListbox.ItemsSource).Add( lLi );


                //G.ccDataLoadInst.saveAndRefresh( );
                //G.ccInformerView.loadAndShow( );
            }
        }


        private void mitAction_Click( object sender, RoutedEventArgs e )
        {
            // -- Daten für das neue KleinWindow
            WinTodoEditData lWinTodoEditData = null;

            // -- Instanz vom neuen KleinWindow
            WinTodoEdit efl = new WinTodoEdit
            {
                Owner = Application.Current.MainWindow,
                ccTitle = "--Win Todo Action--",
                ccHeader = "Action",
                ccText = "Wählt eine Aktion aus. Die Aktion ist das was als nächstes gemacht werden soll. " +
                         "Damit wird im Text ein Marker gesetzt.",
                txtOrdernr = { IsEnabled = false },
                txtGroup = { IsEnabled = false },
                txt_text = { IsEnabled = false }
            };

            // -- Den selectierten Datensatz aus der CollectionView
            Model.StypeTodo lCurrentItem = (Model.StypeTodo)ccCollectionView.CurrentItem;

            // -- dem KleinWindow die Daten zuweisen.
            lWinTodoEditData = SetWindowKleinData( lCurrentItem );

            // -- den Datacontext in dem kleinWindow und die initialen Daten setzen
            efl.stackPanel.DataContext = lWinTodoEditData;

            // -- Anzeigen
            efl.ShowDialog( );

            if ( efl.doSave )
            {
                var lSpaltaText = "";
                switch ( lWinTodoEditData.ccTyp.Substring( 0, 1 ) )
                {
                    case "C":
                        lSpaltaText = "in Plaung";
                        break;
                    case "F":
                        lSpaltaText = "in Arbeit";
                        break;
                    case "X":
                        lSpaltaText = "fertig";
                        break;
                    case "Z":
                        lSpaltaText = "Abgebrochen";
                        break;
                }

                // -- Neuer Typ. Auf diesen Typ wird der Eintrag gesetzt.
                var lNeuTyp = lWinTodoEditData.ccTyp.Substring( 0, 1 );

                // -- Den Typ neu setzen.
                lCurrentItem.Listekopf.Typ = lNeuTyp;

                // -- Die Ordernr neu setzen
                lCurrentItem.Listekopf.Order += 2;   // = lOrderNeu;

                // -- Marker im Text setzen
                var lVu = lCurrentItem.Listedata.Text.Trim( );
                lCurrentItem.Listedata.Text = $"{lVu}\r\r-- {lSpaltaText} {DateTime.Now.ToShortDateString( )}\r";

                G.ccDataLoadInst.saveAndRefresh( );
                G.ccInformerView.loadAndShow( );
            }
        }


        #endregion



        #region ListViewFilter

        private bool TodoFilterTyp( object item )
        {
            string lUiiTyp = (item as Model.StypeTodo)?.Listekopf.Typ;

            // -- Wenn keine Einträge in den Suchfeldern erfolgt sind
            if ( String.IsNullOrEmpty( txtSeekTyp.Text ) )
                return true;

            var a1 = lUiiTyp.ToUpper( ).IndexOfAny( txtSeekTyp.Text.ToUpper( ).ToCharArray( ) );

            return a1 >= 0;
        }


        private bool TodoFilterGroup( object item )
        {
            string lUiiGroup = (item as Model.StypeTodo)?.Listekopf.Group;

            // -- Wenn keine Einträge in den Suchfeldern erfolgt sind
            //if ( String.IsNullOrEmpty( txtSeekGroup.Text ) )
            //    return true;

            var a2 = lUiiGroup.IndexOf( txtSeekGroup.Text, StringComparison.OrdinalIgnoreCase );

            //return ((item as Model.StypeTodo).Listekopf.Text.IndexOf(txtFilter.Text, StringComparison.CurrentCultureIgnoreCase) >= 0);
            return a2 >= 0;
        }


        private bool TodoFilterText( object item )
        {
            string lUiiText = (item as Model.StypeTodo)?.Listekopf.Text;

            // -- Wenn keine Einträge in den Suchfeldern erfolgt sind
            //if ( String.IsNullOrEmpty( txtSeekText.Text ) )
            //    return true;

            var a3 = lUiiText.IndexOf( txtSeekText.Text, StringComparison.OrdinalIgnoreCase );

            //return ((item as Model.StypeTodo).Listekopf.Text.IndexOf(txtFilter.Text, StringComparison.CurrentCultureIgnoreCase) >= 0);
            return a3 >= 0;
        }

        private void txtFilter_TextChanged( object sender, TextChangedEventArgs e )
        {
            CollectionViewSource.GetDefaultView( lboxListbox.ItemsSource ).Refresh( );
        }


        #endregion


        #region private

        private WinTodoEditData SetWindowKleinData( Model.StypeTodo xSelectKey )
        {
            var ptt = new WinTodoEditData
            {
                ccOrder = xSelectKey.Listekopf.Order,
                ccText = xSelectKey.Listekopf.Text,
                ccGroup = xSelectKey.Listekopf.Group
            };

            string such = xSelectKey.Listekopf.Typ;

            foreach ( var lii in ptt.ccTypeList )
            {
                if ( lii.StartsWith( such ) )
                {
                    ptt.ccTyp = lii;
                    break;
                }
            }

            return ptt;
        }


        #endregion

    }



    public class GroupFilter
    {
        private List<Predicate<object>> _filters;

        public Predicate<object> Filter { get; private set; }

        public GroupFilter()
        {
            _filters = new List<Predicate<object>>( );
            Filter = InternalFilter;
        }

        private bool InternalFilter( object o )
        {
            foreach ( var filter in _filters )
            {
                if ( !filter( o ) )
                {
                    return false;
                }
            }
            return true;
        }

        public void AddFilter( Predicate<object> filter )
        {
            _filters.Add( filter );
        }

        public void RemoveFilter( Predicate<object> filter )
        {
            if ( _filters.Contains( filter ) )
            {
                _filters.Remove( filter );
            }
        }
    }


    public class WinTodoConfigData
    {
        public List<string> ccTypeList { get; set; }

        public string ccTyp { get; set; }
        public int ccOrder { get; set; }
        public string ccGroup { get; set; }
        public string ccText { get; set; }


        public WinTodoConfigData()
        {
            ccTypeList = new List<String>( )
            {
                "' ' => Leer",
                "A => Datum",
                "C => Geplant",
                "F => in Arbeit",
                "X => Fertig",
                "Z => Abgebrochen"
            };
            ccOrder = 5;
            ccGroup = "";
            ccText = "";
        }

    }


    public class WinTodoEditData
    {
        public List<string> ccTypeList { get; set; }

        public string ccTyp { get; set; }
        public int ccOrder { get; set; }
        public string ccGroup { get; set; }
        public string ccText { get; set; }


        public WinTodoEditData()
        {
            ccTypeList = new List<String>( )
            {
                "' ' => Leer",
                "A => Datum",
                "C => Geplant",
                "F => in Arbeit",
                "X => Fertig",
                "Z => Abgebrochen"
            };
            ccOrder = 5;
            ccGroup = "";
            ccText = "";
        }

    }


    [Obsolete]
    public class WinTodoActionData
    {
        public List<string> ccTypeList { get; set; }

        public string ccTyp { get; set; }
        public int ccOrder { get; set; }
        public string ccText { get; set; }


        public WinTodoActionData()
        {
            ccTypeList = new List<String>( )
            {
                "C => Geplant",
                "F => in Arbeit",
                "X => Fertig",
                "Z => Abgebrochen"
            };
            ccOrder = 5;
            ccText = "ein Text";
        }

    }


}
