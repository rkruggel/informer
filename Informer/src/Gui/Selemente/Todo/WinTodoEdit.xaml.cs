﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace Informer.Gui.Selemente.Todo
{
    /// <summary>
    /// Interaktionslogik für WinTodoEdit.xaml
    /// </summary>
    public partial class WinTodoEdit : Window
    {
        #region Center Window 

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport( "user32.dll", SetLastError = true )]
        private static extern int GetWindowLong( IntPtr hWnd, int nIndex );
        [DllImport( "user32.dll" )]
        private static extern int SetWindowLong( IntPtr hWnd, int nIndex, int dwNewLong );
        
        #endregion

        public bool doSave = false;

        public string ccTitle { get; set; }
        public string ccHeader { get; set; }
        public string ccText { get; set; }

        public WinTodoEdit()
        {
            InitializeComponent( );
        }

        #region Buttons

        private void btnAbbruch_Click( object sender, RoutedEventArgs e )
        {
            Close( );
        }

        private void btnOk_Click( object sender, RoutedEventArgs e )
        {
            doSave = true;
            Close( );
        }

        #endregion


        #region Center Window 

        /// <summary>
        /// Schliessen-Button ausblenden
        /// <see cref="https://entwickler.de/online/windowsdeveloper/wpf-schliessen-button-ausblenden-579758068.html"/>
        /// </summary>
        /// <code>
        ///   WinItemsEdit lWin = new WinItemsEdit( );
        ///   lWin.Owner = Application.Current.MainWindow;
        ///   lWin.ShowDialog( );
        /// </code>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Loaded( object sender, RoutedEventArgs e )
        {
            var hwnd = new WindowInteropHelper( this ).Handle;
            SetWindowLong( hwnd, GWL_STYLE, GetWindowLong( hwnd, GWL_STYLE ) & ~WS_SYSMENU );
            centerOwner( );
        }

        /// <summary>
        /// Zentriert das Fenster in der mitte von dem Mainwindow.
        /// 
        /// <see cref="https://stackoverflow.com/questions/43799964/center-dynamic-size-window-in-parent-window"/>
        /// </summary>
        private void centerOwner()
        {
            if ( Owner != null )
            {
                double top = Owner.Top + ((Owner.Height - this.ActualHeight) / 2);
                double left = Owner.Left + ((Owner.Width - this.ActualWidth) / 2);

                this.Top = top < 0 ? 0 : top;
                this.Left = left < 0 ? 0 : left;
            }
        }

        #endregion


        private void Grid_Loaded( object sender, RoutedEventArgs e )
        {
            Title = ccTitle;
            txtInfoHead.Text = ccHeader;
            txtInfoText.Text = ccText;
        }


    }
}
