﻿using System.Windows;
using System.Windows.Controls;


namespace Informer.Gui.Uc
{
    /// <summary>
    /// Interaktionslogik für ucTextbox.xaml
    /// </summary>
    public partial class ucTextbox : UserControl
    {
        public ucTextbox()
        {
            InitializeComponent();

#if RELEASE
            btnTextbox.Visibility = Visibility.Collapsed;
#endif
        }

        private void txtTextbox_TextChanged( object sender, TextChangedEventArgs e )
        {

        }

        private void Button_Click( object sender, System.Windows.RoutedEventArgs e )
        {
            Model.Selemente ss = (Model.Selemente)txtTextbox.DataContext;
            ss.Item.Sdata.StypeString += "-";
        }
    }
}
