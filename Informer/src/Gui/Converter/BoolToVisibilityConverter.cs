using System;
using System.Windows;
using System.Windows.Data;

namespace Informer.Gui.Converter
{
    public class BoolToVisibilityConverter : IValueConverter {

        public object Convert( object value, Type xTargetType,
            object parameter, System.Globalization.CultureInfo culture ) {
            bool param = bool.Parse(parameter as string);
            bool val = (bool)value;

            return val == param ? Visibility.Visible : Visibility.Hidden;
            }

        public object ConvertBack( object value, Type xTargetType,
            object parameter, System.Globalization.CultureInfo culture ) {
            return null;
            }
    }
}
