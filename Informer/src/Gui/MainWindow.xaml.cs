﻿using Informer.Utils;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;


namespace Informer.Gui
{

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

#if  DEBUG
        private const string CONFIG_TYPE = "Debug";
#elif RELEASE
        private const string CONFIG_TYPE = "Release";
#endif

        //// -- alte version
        ////private static InformerView ccPfrmReadmeView;
        ////public static InformerView pfrmReadmeView
        ////{
        ////    get
        ////    {
        ////        if ( ccPfrmReadmeView == null )
        ////        {
        ////            ccPfrmReadmeView = new InformerView( );
        ////        }
        ////        return ccPfrmReadmeView;
        ////    }
        ////}

        //// -- neue Version
        //private static InformerView _informerView;
        //public static InformerView ccInformerView => _informerView ?? (_informerView = new InformerView( ));


        public MainWindow()
        {
            G.ccStatusbarData = new Model.StatusbarData( );

            //-- Den Namen des aktuellen Dir
            var lStartFile = (string)Application.Current.Properties["ArbitraryArgName"];

            if ( lStartFile == null )
            {
                var lDevelopDir = Path.GetDirectoryName( Path.GetDirectoryName( Directory.GetCurrentDirectory( ) ) );

                if ( lDevelopDir == null ) throw new ArgumentNullException( "lDevelopDir" );
                lStartFile = Path.Combine(
                    Directory.GetParent( lDevelopDir ).FullName,
                    "DevelopData",
                    Properties.Resources.disFile );
            }

            G.startFullFile = lStartFile;

            // -- System Initialisierung
            InitializeComponent( );

            setWindowsPos( );

            // -- Den Dis-file lesen und in eine View packen und hier anzeigen.
            mainGrid.Children.Add( G.ccInformerView );

            Title = TitleVersion.getit( CONFIG_TYPE ) + "   .NET Client: " + FrameworkVersion.getFameworkVersion( );

            stbMain.DataContext = G.ccStatusbarData;

            G.ccStatusbarData.startdir = G.startFullFile;
            G.ccStatusbarData.savepoints = 0;
            G.ccStatusbarData.configType = CONFIG_TYPE;
            
        }


        private void Window_Loaded( object sender, RoutedEventArgs e )
        {
            //txtStartDir.Content = G.startFullFile;
        }

        /// <summary>
        /// Windows größe und Position setzen.
        /// Muss direkt nach der Initialisieung aufgerufen werden sonst flackert 
        /// das Fenster.
        /// </summary>
        private void setWindowsPos()
        {
            // -- Windowsgröße und position setzen
            Width = Properties.Settings.Default.ccWidth;
            Height = Properties.Settings.Default.ccHeight;
            Top = Properties.Settings.Default.ccTop;
            Left = Properties.Settings.Default.ccLeft;

            if ( Left < 0 || Left > SystemParameters.PrimaryScreenWidth )
            {
                Left = 0;
            }
            if ( Top < 0 || Top > SystemParameters.PrimaryScreenHeight )
            {
                Top = 0;
            }
            if ( Width < 0 || Width > SystemParameters.PrimaryScreenWidth )
            {
                Width = 750;
            }
            if ( Height < 0 || Height > SystemParameters.PrimaryScreenHeight )
            {
                Height = 400;
            }
        }


        private void Window_Closing( object sender, CancelEventArgs e )
        {
            // -- Die Fenstergröße speichern
            Properties.Settings.Default.ccWidth = Width;
            Properties.Settings.Default.ccHeight = Height;
            Properties.Settings.Default.ccTop = Top;
            Properties.Settings.Default.ccLeft = Left;

            Properties.Settings.Default.Save( );
        }

        private void MenuItem_Click( object sender, RoutedEventArgs e )
        {
            Application.Current.Shutdown();
        }
    }

    public class theConverter : IValueConverter
    {
        public object Convert( object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var lProcessbar = (double)value;
            SolidColorBrush lBrush;


            if ( lProcessbar <= 4 )
            {
                lBrush = new SolidColorBrush( Colors.GhostWhite );
            }
            else if ( lProcessbar >= 5 && lProcessbar <= 19 )
            {
                lBrush = new SolidColorBrush( Colors.MistyRose );
            }
            else if ( lProcessbar >= 20 && lProcessbar <= 39 )
            {
                lBrush = new SolidColorBrush( Colors.LightSalmon );
            }
            else
            {
                lBrush = new SolidColorBrush( Colors.OrangeRed );
            }

            return lBrush;
        }

        public object ConvertBack( object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            return null;
        }
    }
}

