﻿using System.Collections.Generic;

using Newtonsoft.Json;


namespace Informer.Model
{
    public class InformerJsonDataPartial
    {
    }




    public partial class InformerJsonData
    {
        [JsonProperty( "config" )]
        public Config Config { get; set; }

        [JsonProperty( "selemente" )]
        public List<Selemente> Selemente { get; set; }
    }

    public partial class Selemente
    {
        [JsonProperty( "item" )]
        public Item Item { get; set; }
    }

    public partial class Item
    {
        [JsonProperty( "name" )]
        public string Name { get; set; }

        [JsonProperty( "id" )]
        public string Id { get; set; }

        [JsonProperty( "steuertype" )]
        public string Steuertype { get; set; }

        [JsonProperty( "attribute" )]
        public Attribute Attribute { get; set; }

        [JsonProperty( "sdata" )]
        public Sdata Sdata { get; set; }
    }

    public partial class Sdata
    {
        [JsonProperty( "stype_string" )]
        public string StypeString { get; set; }

        [JsonProperty( "stype_liste" )]
        public string StypeListe { get; set; }

        [JsonProperty( "stype_todo" )]
        public List<StypeTodo> StypeTodo { get; set; }
    }

    public partial class StypeTodo
    {
        [JsonProperty( "listekopf" )]
        public Listekopf Listekopf { get; set; }

        [JsonProperty( "listedata" )]
        public Listedata Listedata { get; set; }
    }

    public partial class Listekopf
    {
        [JsonProperty( "typ" )]
        public string Typ { get; set; }

        [JsonProperty( "id" )]
        public string Id { get; set; }

        [JsonProperty( "order" )]
        public long Order { get; set; }

        [JsonProperty( "group" )]
        public string Group { get; set; }

        [JsonProperty( "text" )]
        public string Text { get; set; }
    }

    public partial class Listedata
    {
        [JsonProperty( "text" )]
        public string Text { get; set; }
    }

    public partial class Attribute
    {
        [JsonProperty( "len" )]
        public long Len { get; set; }

        [JsonProperty( "vpos" )]
        public long Vpos { get; set; }

        [JsonProperty( "hpos" )]
        public long Hpos { get; set; }
    }

    public partial class Config
    {
        [JsonProperty( "type" )]
        public string Type { get; set; }

        [JsonProperty( "author" )]
        public string Author { get; set; }

        [JsonProperty( "dateofcreate" )]
        public string Dateofcreate { get; set; }

        [JsonProperty( "editor" )]
        public string Editor { get; set; }

        [JsonProperty( "beschreibung" )]
        public string Beschreibung { get; set; }
    }



}
