﻿/*


{
  "config": {
    "type": "docu",
    "author": "Roland Kruggel",
    "dateofcreate": "01.12.2017",
    "editor": "C:\\Program Files\\Sublime Text 3\\subl.exe",
    "beschreibung": "Auto Init"
  },
  "selemente": [
    {
      "item": {
        "pid": {
          "idname": "Label",
          "id": 1980816511
        },
        "steuertype": "Label",
        "label": "",
        "default": "",
        "valide": "",
        "format": "",
        "todokopf": [ ],
        "attribute": {
          "len": 1,
          "vpos": 2,
          "hpos": 2
        },
        "sdata": {}
      }
    },
    {
      "item": {
        "pid": {
          "idname": "Titel",
          "id": 1980816511
        },
        "steuertype": "String",
        "label": "",
        "default": "",
        "valide": "",
        "format": "",
        "todokopf": [ ],
        "attribute": {
          "len": 1,
          "vpos": 2,
          "hpos": 2
        },
        "sdata": {
          "stype_string": "Auto Init",
          "stype_liste": null,
          "stype_todo": null
        }
      }
    },
    {
      "item": {
        "pid": {
          "idname": "Personen",
          "id": 1980922003
        },
        "steuertype": "Liste",
        "label": "",
        "default": "",
        "valide": "",
        "format": "",
        "todokopf": [ ],
        "attribute": {
          "len": 4,
          "vpos": 4,
          "hpos": 2
        },
        "sdata": {
          "stype_string": null,
          "stype_liste": "Roland Kruggel;Maintainer\r\nThomas Mischka;Tester\r\nTheo;Prorammer",
          "stype_todo": null
        }
      }
    },
    {
      "item": {
        "pid": {
          "idname": "Buchungsnummer",
          "id": 1980922003
        },
        "steuertype": "string",
        "label": "",
        "default": "",
        "valide": "",
        "format": "",
        "todokopf": [ ],
        "attribute": {
          "len": 1,
          "vpos": 6,
          "hpos": 2
        },
        "sdata": {
          "stype_string": "12345",
          "stype_liste": null,
          "stype_todo": null
        }
      }
    },
    {
      "item": {
        "pid": {
          "idname": "Todos",
          "id": 1980923963
        },
        "steuertype": "Todo",
        "label": "",
        "default": "",
        "valide": "",
        "format": "",
        "todokopf": [
          {"name": "p1", "head": "Typ", 
           "type": "String", "format":"X", 
           "default":"C", "isseek": true,
           "valide": ["A","C","F","X","Z"]},
          {"name": "p2", "head": "Order",
           "type": "String", "format": "##",
           "default": "00", "isorder":true,
           "valide": ["#Number"]},
          {"name": "p3", "head": "Group",
           "type": "String", "format": "",
           "default": "00", 
           "valide": ["#Text"]},
           {"name": "p4", "head": "Text",
           "type": "String", "format": "",
           "default": "00", 
           "valide": ["#Text"]}
        ],
        
        "attribute": {
          "len": 0,
          "vpos": 2,
          "hpos": 4
        },
        "sdata": {
          "stype_string": null,
          "stype_liste": null,
          "stype_todo": [
            {
              "listekopf": {
                "pid": {
                    "idname": null, "id": 2206476365 },
                "typ": "A",
                "order": 0,
                "group": "Init",
                "text": "Auto Init",
                "lidatas": [
                  { "liname": "p1", "litext": "A" },
                  { "liname": "p2", "litext": "00"},
                  { "liname": "p3", "litext": "Init"},
                  { "liname": "p4", "litext": "Auto Init"}
               
                ]
              },
              "listedata": {
                "text": "Automatische Installation\rDieser Eintrag ist nur ein Dummy. Er kann gelöscht werden.\r"
              }
            }
          ]
        }
      }
    }
  ]
}

*/

// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Informer.Model;
//
//    var data = InformerJsonData.FromJson(jsonString);

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Informer.Model
{


    /// <summary>
    /// <code>
    /// [JsonProperty( "config" )]
    /// [JsonProperty( "selemente" )]
    /// </code>
    /// </summary>
    public partial class InformerJsonData : ViewModelBase
    {
        private Config _config;
        private List<Selemente> _selemente;


        [JsonProperty( "config" )]
        public Config Config { get { return _config; } set { _config = value; onPropertyChanged( ); } }


        [JsonProperty( "selemente" )]
        public List<Selemente> Selemente { get { return _selemente; } set { _selemente = value; onPropertyChanged( ); } }

    }

    /// <summary>
    /// <code>
    ///   [JsonProperty( "type" )]
    ///   [JsonProperty( "author" )]
    ///   [JsonProperty( "dateofcreate" )]
    ///   [JsonProperty( "editor" )]
    ///   [JsonProperty( "beschreibung" )]
    /// </code>
    /// </summary>
    public class Config : ViewModelBase
    {
        private string _type;
        private string _author;
        private string _dateofcreate;
        private string _editor;
        private string _beschreibung;


        [JsonProperty( "type" )]
        public string Type { get { return _type; } set { _type = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "author" )]
        public string Author { get { return _author; } set { _author = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "dateofcreate" )]
        public string Dateofcreate { get { return _dateofcreate; } set { _dateofcreate = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "editor" )]
        public string Editor { get { return _editor; } set { _editor = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "beschreibung" )]
        public string Beschreibung { get { return _beschreibung; } set { _beschreibung = value; onPropertyChanged( ); P += 1; } }

    }




    /// <summary>
    /// <code>
    /// [JsonProperty( "item" )]
    /// </code>
    /// </summary>
    public class Selemente : ViewModelBase
    {
        private Item _item;


        [JsonProperty( "item" )]
        public Item Item { get => _item; set { _item = value; onPropertyChanged( ); P += 1; } }

    }


    /// <summary>
    /// <code>
    ///   [JsonProperty( "name" )]
    ///   [JsonProperty( "id" )]
    ///   [JsonProperty( "steuertype" )]
    ///   [JsonProperty( "attribute" )]
    ///   [JsonProperty( "sdata" )]
    /// </code>
    /// </summary>
    public class Item : ViewModelBase
    {
        private Pid _pid;           //neu
        private string _steuertype;
        private string _label;      //neu
        private string _default;    //neu
        private string _valide;     //neu
        private string _format;     //neu
        private List<Todokopf> _todokopf;
        private Attribute _attribute;
        private Sdata _sdata;


        [JsonProperty( "pid" )]
        public Pid Pid { get { return _pid; } set { _pid = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "steuertype" )]
        public string Steuertype { get { return _steuertype; } set { _steuertype = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "label" )]
        public string Label { get { return _label; } set { _label = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "default" )]
        public string Default { get { return _default; } set { _default = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "valide" )]
        public string Valide { get { return _valide; } set { _valide = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "format" )]
        public string Format { get { return _format; } set { _format = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "todokopf" )]
        public List<Todokopf> Todokopf { get { return _todokopf; } set { _todokopf = value; onPropertyChanged( ); P += 1; } }



        [JsonProperty( "attribute" )]
        public Attribute Attribute { get { return _attribute; } set { _attribute = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "sdata" )]
        public Sdata Sdata { get { return _sdata; } set { _sdata = value; onPropertyChanged( ); P += 1; } }

    }


    /// <summary>
    /// <code>
    ///   [JsonProperty( "len" )]
    ///   [JsonProperty( "vpos" )]
    ///   [JsonProperty( "hpos" )]
    /// </code>
    /// </summary>
    public class Attribute : ViewModelBase
    {
        private long _len;
        private long _vpos;
        private long _hpos;


        [JsonProperty( "len" )]
        public long Len { get { return _len; } set { _len = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "vpos" )]
        public long Vpos { get { return _vpos; } set { _vpos = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "hpos" )]
        public long Hpos { get { return _hpos; } set { _hpos = value; onPropertyChanged( ); P += 1; } }

    }



    public class Pid : ViewModelBase
    {
        private string _idname;
        private long _id;


        [JsonProperty( "idname" )]
        public string Idname { get { return _idname; } set { _idname = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "id" )]
        public long Id { get { return _id; } set { _id = value; onPropertyChanged( ); P += 1; } }

    }



    /// <summary>
    /// <code>
    ///   [JsonProperty( "stype_string" )]
    ///   [JsonProperty( "stype_liste" )]
    ///   [JsonProperty( "stype_todo" )]
    /// </code>
    /// </summary>
    public class Sdata : ViewModelBase
    {
        private string _stypestring;
        private string _stypeliste;
        private List<StypeTodo> _stypetodo;


        [JsonProperty( "stype_string" )]
        public string StypeString { get { return _stypestring; } set { _stypestring = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "stype_liste" )]
        public string StypeListe { get { return _stypeliste; } set { _stypeliste = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "stype_todo" )]
        public List<StypeTodo> StypeTodo { get { return _stypetodo; } set { _stypetodo = value; onPropertyChanged( ); P += 1; } }

    }


    /// <summary>
    /// <code>
    ///   [JsonProperty( "listekopf" )]
    ///   [JsonProperty( "listedata" )]
    /// </code>
    /// </summary>
    public class StypeTodo : ViewModelBase
    {
        private bool _isOccupied;
        private Listekopf _listekopf;
        private Listedata _listedata;


        [JsonIgnore( )]
        public bool IsOccupied { get { return _isOccupied; } set { _isOccupied = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "listekopf" )]
        public Listekopf Listekopf { get { return _listekopf; } set { _listekopf = value; onPropertyChanged( ); P += 1; } }


        [JsonProperty( "listedata" )]
        public Listedata Listedata { get { return _listedata; } set { _listedata = value; onPropertyChanged( ); P += 1; } }

    }


    /// <summary>
    /// <code>
    ///   [JsonProperty( "text" )]
    /// </code>
    /// </summary>
    public class Listedata : ViewModelBase
    {
        private string _text;


        [JsonProperty( "text" )]
        public string Text { get { return _text; } set { _text = value; onPropertyChanged( ); P += 1; } }

    }


    /// <summary>
    /// <code>
    ///   [JsonProperty( "typ" )]
    ///   [JsonProperty( "id" )]
    ///   [JsonProperty( "order" )]
    ///   [JsonProperty( "group" )]
    ///   [JsonProperty( "text" )]
    /// </code>
    /// </summary>
    public class Listekopf : ViewModelBase
    {
        private Pid _pid;
        private string _typ;
        private string _id;
        private int _order;
        private string _group;
        private string _text;
        private List<Lidata> _lidatas;


        [JsonProperty( "pid" )]
        public Pid Pid { get { return _pid; } set { _pid = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "typ" )]
        public string Typ { get { return _typ; } set { _typ = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "order" )]
        public int Order { get { return _order; } set { _order = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "group" )]
        public string Group { get { return _group; } set { _group = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "text" )]
        public string Text { get { return _text; } set { _text = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "lidatas" )]
        public List<Lidata> Lidatas { get { return _lidatas; } set { _lidatas = value; onPropertyChanged( ); P += 1; } }

    }


    public class Lidata : ViewModelBase
    {
        private string _liname;
        private string _litext;



        [JsonProperty( "liname" )]
        public string Liname { get { return _liname; } set { _liname = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "litext" )]
        public string Litext { get { return _litext; } set { _litext = value; onPropertyChanged( ); P += 1; } }

    }


    public class Todokopf : ViewModelBase
    {
        private string _name;
        private string _head;
        private string _type;
        private string _format;
        private string _default;
        private bool? _isseek;
        private List<string> _valide;
        private bool? _isorder;



        [JsonProperty( "name" )]
        public string Name { get { return _name; } set { _name = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "head" )]
        public string Head { get { return _head; } set { _head = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "type" )]
        public string Type { get { return _type; } set { _type = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "format" )]
        public string Format { get { return _format; } set { _format = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "default" )]
        public string Default { get { return _default; } set { _default = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "isseek" )]
        public bool? Isseek { get { return _isseek; } set { _isseek = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "valide" )]
        public List<string> Valide { get { return _valide; } set { _valide = value; onPropertyChanged( ); P += 1; } }

        [JsonProperty( "isorder" )]
        public bool? Isorder { get { return _isorder; } set { _isorder = value; onPropertyChanged( ); P += 1; } }


    }


    #region Funktionen

    public partial class InformerJsonData
    {
        public static InformerJsonData FromJson( string json ) => JsonConvert.DeserializeObject<InformerJsonData>( json, Converter.Settings );
    }


    public static class Serialize
    {
        public static string ToJson( this InformerJsonData self ) => JsonConvert.SerializeObject( self, Converter.Settings );
    }

    public class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }

    #endregion

}
