﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

using Informer.Annotations;

using Newtonsoft.Json;

namespace Informer.Model
{

    public enum ESteuerTyps
    {
        None, String, Integer, Liste, Todo
    }


    public class ViewModelBase: INotifyPropertyChanged
    {
        [JsonIgnore( )]
        public int P
        {
            get => G.ccStatusbarData.savepoints;
            set { G.ccStatusbarData.savepoints = value; }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        public virtual void onPropertyChanged( [CallerMemberName] string xPropertyName = null )
        {
            PropertyChanged?.Invoke( this, new PropertyChangedEventArgs( xPropertyName ) );
        }
    }
}

