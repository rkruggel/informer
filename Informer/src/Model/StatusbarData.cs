﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Informer.Model
{
    public class StatusbarData : ViewModelBase
    {
        private string _startdir;
        private string _configType;
        private bool _isSave;
        private int _savepoints = 0;
        private string _eff = "f";
        private string _ue = "ue";
        private string _zik = "zik";


        public string startdir { get { return _startdir; } set { _startdir = value; onPropertyChanged( ); } }

        public string configType { get => _configType; set { _configType = value; onPropertyChanged( ); } }

        public bool isSave { get => _isSave; set { _isSave = value; onPropertyChanged( ); } }

        public int savepoints { get => _savepoints; set { _savepoints = value; onPropertyChanged( ); } }
        public string eff { get => _eff; set { _eff = value; onPropertyChanged( ); } }
        public string ue { get => _ue; set { _ue = value; onPropertyChanged( ); } }
        public string zik { get => _zik; set { _zik = value; onPropertyChanged( ); } }


    }
}
