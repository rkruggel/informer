﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Informer.src.Model
{
    class Class1
    {
    }


    public class Rootobject
    {
        public Config config { get; set; }
        public Selemente[] selemente { get; set; }
    }

    public class Config
    {
        public string type { get; set; }
        public string author { get; set; }
        public string dateofcreate { get; set; }
        public string editor { get; set; }
        public string beschreibung { get; set; }
    }

    public class Selemente
    {
        public Item item { get; set; }
    }

    public class Item
    {
        public Pid pid { get; set; }
        public string steuertype { get; set; }
        public string label { get; set; }
        public string _default { get; set; }
        public string valide { get; set; }
        public string format { get; set; }
        public Todokopf[] todokopf { get; set; }
        public Attribute attribute { get; set; }
        public Sdata sdata { get; set; }
    }

    public class Pid
    {
        public string idname { get; set; }
        public int id { get; set; }
    }

    public class Attribute
    {
        public int len { get; set; }
        public int vpos { get; set; }
        public int hpos { get; set; }
    }

    public class Sdata
    {
        public string stype_string { get; set; }
        public string stype_liste { get; set; }
        public Stype_Todo[] stype_todo { get; set; }
    }

    public class Stype_Todo
    {
        public Listekopf listekopf { get; set; }
        public Listedata listedata { get; set; }
    }

    public class Listekopf
    {
        public Pid1 pid { get; set; }
        public string typ { get; set; }
        public int order { get; set; }
        public string group { get; set; }
        public string text { get; set; }
        public Lidata[] lidatas { get; set; }
    }

    public class Pid1
    {
        public object idname { get; set; }
        public long id { get; set; }
    }

    public class Lidata
    {
        public string liname { get; set; }
        public string litext { get; set; }
    }

    public class Listedata
    {
        public string text { get; set; }
    }

    public class Todokopf
    {
        public string name { get; set; }
        public string head { get; set; }
        public string type { get; set; }
        public string format { get; set; }
        public string _default { get; set; }
        public bool isseek { get; set; }
        public string[] valide { get; set; }
        public bool isorder { get; set; }
    }



}
