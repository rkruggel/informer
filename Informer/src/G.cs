﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Informer.Gui;
using Informer.Utils;

namespace Informer
{


    /// <summary>
    /// Contains global variables for project.
    /// </summary>
    public static class G
    {
        /// <summary>
        /// Das Startdir incl. Filename
        /// Bspl.:
        ///   C:\Users\rkruggel\Desktop\README.dis
        /// </summary>
        public static string startFullFile { get; set; }

        /// <summary>
        /// Das Startdir
        /// Bspl.:
        ///   C:\Users\rkruggel\Desktop
        /// </summary>
        public static string startDir => Path.GetDirectoryName( startFullFile );

        /// <summary>
        /// Das File aus dem Startdir
        /// Bspl.:
        ///   README.dis
        /// </summary>
        public static string startFile => Path.GetFileName( startFullFile );


        public static DataLoad ccDataLoadInst { get; set; }

        public static Model.StatusbarData ccStatusbarData { get; set; }

        // -- alte version
        //private static InformerView ccPfrmReadmeView;
        //public static InformerView pfrmReadmeView
        //{
        //    get
        //    {
        //        if ( ccPfrmReadmeView == null )
        //        {
        //            ccPfrmReadmeView = new InformerView( );
        //        }
        //        return ccPfrmReadmeView;
        //    }
        //}

        // -- neue Version
        private static InformerView _informerView;

        public static InformerView ccInformerView => _informerView ?? (_informerView = new InformerView( ));

        public static List<Model.Selemente> ccMainSelementeData => ccDataLoadInst.informerMainData.Selemente;

        public static Model.Config ccMainConfigData => ccDataLoadInst.informerMainData.Config;

    }
}


