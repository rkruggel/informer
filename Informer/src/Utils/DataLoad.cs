﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Windows;
using System.Windows.Media.Media3D;
using System.Xml.Schema;

using Informer.Model;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;


namespace Informer.Utils
{
    public class DataLoad
    {

        public enum EStatus
        {
            None, Save, Load, Init
        }

        #region Attribute

        /// <summary>
        /// Enthält die gesamten Daten des Json-File (.dis-File)
        /// </summary>
        public Model.InformerJsonData informerMainData { get; set; }

        public EStatus ccStatus { get; set; }

        #endregion

        #region .ctor

        public DataLoad() { }

        #endregion


        #region public --------------------------------------------------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <see cref="https://app.quicktype.io/#r=json2csharp"/>
        public void load()
        {
            // -- Den Status zurücksetzen
            ccStatus = EStatus.None;

            string lFo = "";

            // -- Daten aus dem File lesen
            try
            {
                lFo = File.ReadAllText( G.startFullFile );
            }
            catch ( Exception ex )
            {
                Libs.message( "hair", xEx: ex, xIcon: Libs.MbImage.err );
                Environment.Exit( 1 );
            }

            // -- Die gesamten gelesenen Daten in das Object InformerJsonData deserialisieren
            Model.InformerJsonData lImd = Model.InformerJsonData.FromJson( lFo );

            // -- Test ob ein neuer .dis-File initiiert werden soll.
            if ( lImd == null )
            {
                ccStatus = EStatus.Init;
                return;
            }


            // -- Die einfachen Steuerelemente sortieren.
            lImd.Selemente = (from p in lImd.Selemente
                              where p.Item.Attribute.Hpos >= 0
                              orderby p.Item.Attribute.Hpos, p.Item.Attribute.Vpos
                              select p).ToList( );


            // -- Die Todo der fertigen Objects sortieren
            for ( int i = 0; i < lImd.Selemente.Count; i++ )
            {
                Model.Selemente lElem = lImd.Selemente[i];

                if ( lElem.Item.Sdata.StypeTodo != null )
                {
                    lElem.Item.Sdata.StypeTodo = (from p in lElem.Item.Sdata.StypeTodo
                                                  orderby p.Listekopf.Typ, p.Listekopf.Order
                                                  select p).ToList( );
                    foreach ( Model.StypeTodo lListe in lElem.Item.Sdata.StypeTodo )
                    {
                        lListe.Listedata.Text = lListe.Listedata.Text.TrimEnd( ) + "\r";
                    }
                }
            }

            this.informerMainData = lImd;

            G.ccStatusbarData.savepoints = 0;
            G.ccStatusbarData.isSave = true;

            ccStatus = EStatus.Load;
        }

        public void initDisFile()
        {
            var jsonstring = @"
{
  'config': {
    'type': 'docu',
    'author': 'Roland Kruggel',
    'dateofcreate': '01.12.2017',
    'editor': 'C:\\Program Files\\Sublime Text 3\\subl.exe',
    'beschreibung': 'Auto Init'
  },
  'selemente': [
    {
      'item': {
        'pid': {
          'idname': 'Label',
          'id': 1980816511
        },
        'steuertype': 'Label',
        'label': '',
        'default': '',
        'valide': '',
        'format': '',
        'attribute': {
          'len': 1,
          'vpos': 2,
          'hpos': 2
        },
        'sdata': {}
      }
    },
    {
      'item': {
        'pid': {
          'idname': 'Titel',
          'id': 1980816511
        },
        'steuertype': 'String',
        'label': '',
        'default': '',
        'valide': '',
        'format': '',
        'attribute': {
          'len': 1,
          'vpos': 2,
          'hpos': 2
        },
        'sdata': {
          'stype_string': 'Auto Init',
          'stype_liste': null,
          'stype_todo': null
        }
      }
    },
    {
      'item': {
        'pid': {
          'idname': 'Personen',
          'id': 1980922003
        },
        'steuertype': 'Liste',
        'label': '',
        'default': '',
        'valide': '',
        'format': '',
        'attribute': {
          'len': 4,
          'vpos': 4,
          'hpos': 2
        },
        'sdata': {
          'stype_string': null,
          'stype_liste': 'Roland Kruggel;Maintainer\r\nThomas Mischka;Tester\r\nTheo;Prorammer',
          'stype_todo': null
        }
      }
    },
    {
      'item': {
        'pid': {
          'idname': 'Buchungsnummer',
          'id': 1980922003
        },
        'steuertype': 'string',
        'label': '',
        'default': '',
        'valide': '',
        'format': '',
        'attribute': {
          'len': 1,
          'vpos': 6,
          'hpos': 2
        },
        'sdata': {
          'stype_string': '12345',
          'stype_liste': null,
          'stype_todo': null
        }
      }
    },
    {
      'item': {
        'pid': {
          'idname': 'Todos',
          'id': 1980923963
        },
        'steuertype': 'Todo',
        'label': '',
        'default': '',
        'valide': '',
        'format': '',
        'attribute': {
          'len': 0,
          'vpos': 2,
          'hpos': 4
        },
        'sdata': {
          'stype_string': null,
          'stype_liste': null,
          'stype_todo': [
            {
              'listekopf': {
                'pid': {
                    'idname': null, 'id': 2206476365 },
                'typ': 'A',
                'order': 0,
                'group': 'Init',
                'text': 'Auto Init'
              },
              'listedata': {
                'text': 'Automatische Installation\rDieser Eintrag ist nur ein Dummy. Er kann gelöscht werden.\r'
              }
            }
          ]
        }
      }
    }
  ]
}
";







            this.informerMainData = JsonConvert.DeserializeObject<Model.InformerJsonData>( jsonstring );

            // -- speichern
            save( );
        }

        public void initDisFile2()
        {
            //var db = LiteDB.
        }


        public void save()
        {
            void SortHpos( List<Model.Selemente> xSelemente )
            {
                Int16 lco = 0;

                // -- Die Sortierreihenfolgen setzen.  
                //    Die hpos setzen.
                // Iteriert über die hpos
                var lHpos1 = (from p in xSelemente
                              orderby p.Item.Attribute.Hpos
                              select p.Item.Attribute.Hpos).Distinct( );

                lco = 0;
                foreach ( var lHp in lHpos1 )      // iteration über die beiden hpos/spalten
                {
                    var gshh = from o in this.informerMainData.Selemente
                               where o.Item.Attribute.Hpos == lHp
                               select o;

                    lco += 2;
                    foreach ( var lSelemente in gshh )
                    {
                        lSelemente.Item.Attribute.Hpos = lco;
                    }

                }
            }

            void SortVpos( List<Model.Selemente> xSelemente )
            {
                Int16 co = 0;
                // -- Die Sortierreihenfolgen setzen.  
                //    Die vpos setzen.
                // Iteriert über die hpos
                var usj = (from p in this.informerMainData.Selemente
                           orderby p.Item.Attribute.Hpos
                           select p.Item.Attribute.Hpos).Distinct( );

                foreach ( var lHpos in usj )
                {
                    var gshh = from o in this.informerMainData.Selemente
                               where o.Item.Attribute.Hpos == lHpos
                               orderby o.Item.Attribute.Vpos
                               select o;

                    // Setzt die hpos in zweierschritten
                    co = 0;
                    foreach ( var lSelemente in gshh )
                    {
                        co += 2;
                        lSelemente.Item.Attribute.Vpos = co;

                    }
                }

            }

            void SetId( List<Model.Selemente> xSelemente )
            {
                // -- Wenn ID=0 dann wird die ID neu gesetzt.
                //    Main Data
                Model.Item olk = (from p in xSelemente
                                  where p.Item.Pid.Id == 0
                                  select p.Item).FirstOrDefault( );

                if ( olk != null )
                {
                    olk.Pid = Libs.getPid( xName: olk.Pid.Idname );
                }



                //// -- -------------------------------------------------
                //var olb = from p in xSelemente
                //          where p.Item.Sdata.StypeTodo != null
                //          select p.Item;

                //foreach ( Item lItem in olb )
                //{
                //    List<Todokopf> lLii = new List<Todokopf>( );

                //    lLii.Add( new Todokopf
                //    {
                //        Type = "String",
                //        Format = "",
                //        Default = "",
                //        Isseek = false,
                //        Isorder = false,
                //        Valide = new List<string> { "" },
                //        Name = "p1",
                //        Head = "typ",
                //    } );

                //    lLii.Add( new Todokopf
                //    {
                //        Type = "String",
                //        Format = "",
                //        Default = "",
                //        Isseek = false,
                //        Isorder = false,
                //        Valide = new List<string> { "" },
                //        Name = "p2",
                //        Head = "order",
                //    } );

                //    lLii.Add( new Todokopf
                //    {
                //        Type = "String",
                //        Format = "",
                //        Default = "",
                //        Isseek = false,
                //        Isorder = false,
                //        Valide = new List<string> { "" },
                //        Name = "p3",
                //        Head = "group",
                //    } );

                //    lLii.Add( new Todokopf
                //    {
                //        Type = "String",
                //        Format = "",
                //        Default = "",
                //        Isseek = false,
                //        Isorder = false,
                //        Valide = new List<string> { "" },
                //        Name = "p4",
                //        Head = "text",
                //    } );
                 
                //    lItem.Todokopf = lLii;
                //}


                //// -- ----------------------------------------------------
                //var ola = from p in xSelemente
                //          where p.Item.Sdata.StypeTodo != null
                //          select p.Item.Sdata.StypeTodo;

                //foreach ( List<StypeTodo> lStypeTodos in ola )
                //{
                //    foreach ( StypeTodo lStypeTodo in lStypeTodos )
                //    {
                //        Listekopf k = lStypeTodo.Listekopf;
                //        k.Lidatas = new List<Lidata>();
                //        k.Lidatas.Add( new Lidata { Liname = "p1", Litext = k.Typ } );
                //        k.Lidatas.Add( new Lidata { Liname = "p2", Litext = k.Order.ToString("0#") } );
                //        k.Lidatas.Add( new Lidata { Liname = "p3", Litext = k.Group } );
                //        k.Lidatas.Add( new Lidata { Liname = "p4", Litext = k.Text } );
                        
                //    }
                //}


            }

            // -- Den Status zurücksetzen
            ccStatus = EStatus.None;

            // Sortieren hpos
            SortHpos( this.informerMainData.Selemente );

            // Sortieren vpos
            SortVpos( this.informerMainData.Selemente );

            // Id setzen
            SetId( this.informerMainData.Selemente );


            // -- hier wird aus dem Json-Object ein String
            string lDeser;
            //lDeser = JsonConvert.SerializeObject( this.informerMainData, Formatting.Indented );
            lDeser = Model.Serialize.ToJson( this.informerMainData );


            // -- Check ob das bak-Dir vorhanden ist. Wenn nicht erzegen.
            var lSavePath = Path.GetDirectoryName( G.startFullFile );
            var lBakDir = Path.Combine( lSavePath, "bak" );
            if ( !Directory.Exists( lBakDir ) )
            {
                Directory.CreateDirectory( lBakDir );
            }

            // -- Die Originaldatei kopieren/sichern
            var lTsNummer = Utils.Libs.ccTimestamp;
            var lDestFileName = Path.Combine( lBakDir, Path.GetFileNameWithoutExtension( G.startFullFile ) + "." + lTsNummer );
            File.Copy( G.startFullFile, lDestFileName );

            // -- Speichern
            try
            {
                File.WriteAllText( G.startFullFile, lDeser );
            }
            catch ( Exception ex )
            {
                Libs.message( "fdse", xEx: ex, xIcon: Libs.MbImage.err );
                Environment.Exit( 1 );
            }

            G.ccStatusbarData.savepoints = 0;
            G.ccStatusbarData.isSave = true;

            ccStatus = EStatus.Save;
        }


        public void refresh()
        {
            this.load( );
        }

        public void saveAndRefresh()
        {
            this.save( );
            this.load( );
        }
        #endregion


        #region private --------------------------------------------------------------------

        /// <summary>
        /// Behandelt die File-Config
        /// </summary>
        private void fileConfig()
        {
            // todo: Behandelt die File-Config

        }

        /// <summary>
        /// Behandelt die User-Config
        /// </summary>
        private void userConfig()
        {
            // todo: Behandelt die User-Config

        }

        #endregion

    }
}
