﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

using Informer.Model;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Informer.Utils
{
    class Libs
    {
        public class lbb
        {
            public enum ESteuerTypen { None, String, Liste, Text }

        }


        public enum MbImage
        {
            none = MessageBoxImage.None,
            err = MessageBoxImage.Error,
            hinweis = MessageBoxImage.Information
        }

        private static Int64 ccTimestampRaw => System.Convert.ToInt64( (DateTime.UtcNow - new DateTime( 2017, 12, 10 )).TotalMilliseconds );

        //public static string ccTimestamp => (DateTime.UtcNow - new DateTime( 2017, 12, 10 )).TotalMilliseconds.ToString( "000000000#" );
        public static string ccTimestamp =>  ccTimestampRaw.ToString( "000000000#" );

        public static Pid getPid( Int64 xId = 0, string xName = "" )
        {
            if (xId == 0)
            {
                xId = ccTimestampRaw;
            }

            return new Pid {Id = xId, Idname = xName};
        }

        public static void message( string xId, string xText = null, Exception xEx = null, MbImage xIcon = MbImage.none )
        {
            string lMsgString = "";
            if ( xText != null )
            {
                lMsgString += xText + "\r";
            }
            if ( xEx != null )
            {
                lMsgString += xEx.Message;
            }

            MessageBox.Show( lMsgString, "Informer  -" + xId + "-", MessageBoxButton.OK, (MessageBoxImage)xIcon );
        }


    }
    
}
