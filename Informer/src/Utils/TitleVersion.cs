﻿using System;
using System.Deployment.Application;
using System.IO;
using System.Reflection;
using System.Windows;

using Microsoft.Win32;

// LabelSolutionII v2
//
// Copyright (c) 2011-2017 Werner Turck GmbH & Co. KG
// All rights reserved.
// 
// Idee und Author: Roland Kruggel
// Maintainer: Roland Kruggel


namespace Informer.Utils
{

    /// <summary>
    /// Erzeugt aus dem aktuellen Assembly oder aus der Versions.txt die Turk-Versionsnummer
    /// </summary>
    /// <remarks></remarks>
    public class TitleVersion
    {

        /// <summary>
        /// Construktor
        /// </summary>
        /// <param name="xAssembly">Hier 'Assembly.GetExecutingAssembly()' übergeben</param>
        /// <param name="xDevDoc">Ob die anwendung als DEBUG oder als RELEASE compiliert wurde.</param>
        /// <remarks></remarks>
        public static string getit( Assembly xAssembly, string xDevDoc )
        {
            string lDevDoc = xDevDoc;
            Version lVersionAssembly;

            // Die Version des Assembly
            lVersionAssembly = xAssembly.GetName( ).Version; // => {2.0.5988.26162}

            if ( lVersionAssembly.Minor == 0 && lVersionAssembly.Revision == 0 )
            {
                return makeVersionByVer( );
            }
            else
            {
                return makeVersionByAss( lVersionAssembly, Convert.ToString( lDevDoc ) );
            }
        }


        /// <summary>
        /// Construktor
        /// </summary>
        /// <param name="xDevDoc">Ob die anwendung als DEBUG oder als RELEASE compiliert wurde.</param>
        /// <remarks></remarks>
        public static string getit( string xDevDoc )
        {
            Version lVersionDeploy = null;

            // Die Version des Deplyments (Hier wird die Minor-Number automatisch bei jedem Deployment um eins erhöht)
            if ( ApplicationDeployment.IsNetworkDeployed == true )
            {
                lVersionDeploy = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                return makeVersionByDeploy( lVersionDeploy, xDevDoc );
            }
            else
            {
                return makeVersionByVer( );

            }

        }


        public static string getVersion( bool xWithDatum )
        {
            Version lVer = null;
            string lReturnValue = "";

            try
            {
                lVer = ApplicationDeployment.CurrentDeployment.CurrentVersion;

                if ( xWithDatum )
                {
                    // die Assembly Version
                    var lVersion = Assembly.GetEntryAssembly( ).GetName( ).Version;
                    // das Assembly Datum
                    var lBuildDateTime = new DateTime( 2000, 1, 1 ).Add( new TimeSpan(
                                                                             TimeSpan.TicksPerDay *
                                                                             lVersion
                                                                                     .Build + // days since 1 January 2000
                                                                             TimeSpan.TicksPerSecond * 2 *
                                                                             lVersion.Revision ) );

                    lReturnValue = String.Format( "{0}.{1} [{2}]   {3:dd.MM.yyyy H:mm}",
                                                  lVer.Major, lVer.Minor, lVer.Revision, lBuildDateTime );
                }
                else
                {
                    lReturnValue = String.Format( "{0}.{1} [{2}]",
                                                  lVer.Major, lVer.Minor, lVer.Revision );
                }
            }
            catch ( Exception ex )
            {
                Libs.message( "h6rd", xEx: ex, xIcon: Libs.MbImage.err );
            }

            return lReturnValue;
        }




        /// <summary>
        /// Liest die Versionsnummer aus dem Assembly aus und bereitet die
        ///  Daten zum Anzeigen auf
        /// </summary>
        /// <returns></returns>
        private static string makeVersionByAss( Version xVersion, string xDevDoc )
        {
            var lBuildDate = (new DateTime( 2000, 1, 1 )).Add( new TimeSpan( TimeSpan.TicksPerDay * xVersion.Build + TimeSpan.TicksPerSecond * 2 * xVersion.Revision ) );
            return string.Format( "{0}.{1}  [{2}]   {3} {4}", xVersion.Major, xVersion.Minor, xDevDoc, lBuildDate.ToShortDateString( ), lBuildDate.ToShortTimeString( ) );
        }


        /// <summary>
        /// Liest die 'Version.txt' und bereitet die Daten zum Anzeigen auf
        /// </summary>
        /// <returns></returns>
        private static string makeVersionByVer()
        {

            string line = null;


            // -- ---------------------------------------------------
            // Die Version.txt wird als Eingebettete Resource gespeichert.
            //   Version.txt => Eigenschaften => 
            //     Buildvorgang: Eingebettete Resource
            //     In Ausgabeverzeichnis Kopieren: Immer kopieren
            // siehe: https://dotnet-snippets.de/snippet/inhalt-einer-als-ressource-eingebetteten-text-datei-ausgeben/1424
            //
            // So wird die Datei dann wieder ausgelesen
            string strSchemaPath = Assembly.GetExecutingAssembly( ).GetName( ).Name + ".Version.txt";
            // Die Ressource per Stream auslesen
            Stream objIOStream = Assembly.GetExecutingAssembly( ).GetManifestResourceStream( strSchemaPath );
            line = new StreamReader( objIOStream ).ReadToEnd( );

            // -- ---------------------------------------------------
            // -- Zersplitten und verarbeiten
            var part = line.Split( " ".ToCharArray( ) );

            var lDatum = part[0].Split( ':' )[1];
            var lCounter = part[1].Split( ':' )[1];
            var lVersion = part[2].Split( ':' )[1];
            var lCompile = part[3].Split( ':' )[1];

            var ret = "";
            if ( lCompile.ToUpper( ) == "RELEASE" )
            {
                //ret = string.Format("{0}  ({1})   {2}", lVersion, lCounter, lDatum);
                ret = $"{lVersion}  ({lCounter})   {lDatum}";
            }
            else
            {
                //ret = string.Format("{0}  ({1})  [{2}]   {3}", Convert.ToString(lVersion), lCounter, lCompile, lDatum);
                ret = $"{Convert.ToString( lVersion )}  ({lCounter})  [{lCompile}]   {lDatum}";
            }
            return Convert.ToString( ret );
        }


        private static string makeVersionByDeploy( Version xVersion, string xDevDoc )
        {
            var lVersionAss = Assembly.GetExecutingAssembly( ).GetName( ).Version;

            var lBuildDate = (new DateTime( 2000, 1, 1 )).Add( new TimeSpan( TimeSpan.TicksPerDay * lVersionAss.Build + TimeSpan.TicksPerSecond * 2 * lVersionAss.Revision ) );

            var lDatum = lBuildDate;
            var lCounter = xVersion.Revision;
            var lVersion = string.Format( "{0}.{1}", xVersion.Major, xVersion.Minor );
            var lCompile = xDevDoc;

            var ret = "";
            ret = string.Format( "{0}  ({1:00#})  [{2}]   {3:dd.MM.yyyy H:mm}", Convert.ToString( lVersion ), lCounter, lCompile, lDatum );
            return Convert.ToString( ret );
        }
    }



    public class FrameworkVersion
    {
        /// <summary>
        /// Die .Net version ermitteln.
        /// Unter dem u.g. Link veröffentlich Microsoft die Key's unter denen die Versionen in der Registry
        /// eingetragen sind.
        /// </summary>
        /// <see cref="https://docs.microsoft.com/de-de/dotnet/framework/migration-guide/how-to-determine-which-versions-are-installed"/>
        /// <returns></returns>
        public static string getFameworkVersion()
        {
            string ret = "";
            using ( RegistryKey lNdpKey = RegistryKey.OpenBaseKey( RegistryHive.LocalMachine, RegistryView.Registry32 ).OpenSubKey( "SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\" ) )
            {

                if ( lNdpKey != null && lNdpKey.GetValue( "Release" ) != null )
                {
                    ret = checkFor45DotVersion( Convert.ToInt32( lNdpKey.GetValue( "Release" ) ) );
                }
                else
                {
                    ret = "Version 4.5 or later is not detected.";
                }
            }
            return ret;
        }

        private static string checkFor45DotVersion( int xReleaseKey )
        {
            string retver;
            switch ( xReleaseKey )
            {

                case 378389:
                    retver = "4.5";
                    break;

                case 378675:
                case 378758:
                    retver = "4.5.1";
                    break;

                case 379893:
                    retver = "4.5.2";
                    break;

                case 393295:
                case 393297:
                    retver = "4.6";
                    break;

                case 394254:
                case 394271:
                    retver = "4.6.1";
                    break;

                case 394802:
                case 394806:
                    retver = "4.6.2";
                    break;

                case 460798:
                case 460805:
                    retver = "4.7";
                    break;

                case 461308:
                case 461310:
                    retver = "4.7.1";
                    break;

                default:
                    retver = "No 4.5 or later version detected";
                    break;
            }

            // This line should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            return retver;
        }
    }

}
