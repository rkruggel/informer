using System;
using System.Diagnostics;
using System.Windows;

using Informer.Gui;

namespace Informer.Utils
{
    /// <summary>
    /// Den Editor Aufrufen
    /// </summary>
    public class ExecEditor
    {
        //private readonly InformerView ccInformerView;
        //private InformerFirstlineBean ccInformerFirstlineBean;
        private readonly Process ccProcess = new Process( );

        //public ExecEditor( InformerView xInformerView )
        public ExecEditor( )
        {
            //this.ccInformerView = xInformerView;
        }


        public void execEditor()
        {
            var lInfo = new ProcessStartInfo
            {
                UseShellExecute = false,
                FileName = Properties.Settings.Default.ccCfgEditor,
                Arguments = "\"" + G.startFullFile + "\"",
                CreateNoWindow = true
            };


            ccProcess.StartInfo = lInfo;
            ccProcess.EnableRaisingEvents = true;
            ccProcess.Exited += new EventHandler( myProcess_Exited );

            try
            {
                ccProcess.Start( );
            }
            catch ( Exception ex )
            {
                Libs.message( "fdew", xEx: ex, xIcon: Libs.MbImage.err );
            }
        }

        private void myProcess_Exited( object sender, System.EventArgs e )
        {
            //eventHandled = true;
            var sjs = ccProcess.ExitCode;
        }
    }
}