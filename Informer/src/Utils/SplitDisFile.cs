﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Informer.Utils
{

    /// <summary>
    /// Expandieren der firstline des .dis files. 
    ///  Es enthält im Json-Format wichtige attribute für die Steuerung
    ///  des Programmes. Der Json-Eintrag kann über mehrere Zeilen gehen 
    ///  und wird durch eine Leerzeile von dem Rest der Einträge getrennt. 
    ///  Der Json-Teil darf keine Leerzeilen enthalten.
    /// </summary>
    public class SplitDisFile
    {
        private string[] ccFi { get; set; }
        private string ccFo { get; set; }

        /// <summary>
        /// Der Json-Part des .di_files
        /// </summary>
        public List<string> ccRejson { get; set; }
        /// <summary>
        /// Der Text-Part des .dis-Files
        /// </summary>
        public List<string> ccRetxt { get; set; }


        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="xFi"></param>
        public SplitDisFile( string[] xFi )
        {
            this.ccFi = xFi;
            this.ccFo = String.Join( "\n", xFi );
            splitDi( );
        }

        /// <summary>
        /// .ctor
        /// </summary>
        /// <param name="xFo"></param>
        public SplitDisFile( string xFo )
        {
            this.ccFi = xFo.Split( "\n".ToCharArray( ) );
            this.ccFo = xFo;
            splitDi( );
        }

        /// <summary>
        /// Splittet den Json-Teil von dem Text-Teil
        /// </summary>
        private void splitDi()
        {
            int lTextStartPos = getTextPos( );


            if ( lTextStartPos > 0 )
            {
                this.ccRejson = splitJsonPart( lTextStartPos );
                this.ccRetxt = splitTextPart( lTextStartPos );
            }
            else
            {
                this.ccRejson = splitJsonPart( ccFi.Length );
                this.ccRetxt = new List<string>( ); // {};
            }

        }

        /// <summary>
        /// Die Zeilennummer an der die Texte anfangen.
        /// </summary>
        /// <returns>
        ///    0    => der gesamte .dis-file besteht nur aus Json. Kein Textpart vorhanden.
        ///    1..  => die Position in der der text anfängt.
        /// </returns>
        private int getTextPos()
        {
            int lGetTextPos = 0;
            try
            {
                var lSss = JsonConvert.DeserializeObject( this.ccFo );
            }
            catch ( JsonReaderException ex )
            {

                if ( ex.Message.StartsWith( "Additional text encountered after finished reading JSON content" ) )
                {
                    lGetTextPos = ex.LineNumber;
                }
                else
                {
                    throw new Exception( "Json-Error in .dis-File" );
                }
            }
            catch ( Exception ex )
            {
                throw new Exception( ex.Message );
            }

            return lGetTextPos;
        }

        /// <summary>
        /// Der Json-Part
        /// Der Json-Part muss immer in der ersten Zeile stehen.
        ///  Es  kann mehrzeilig sein.
        /// </summary>
        /// <param name="xTextStartPos"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private List<string> splitJsonPart( int xTextStartPos )
        {
            var lJsonlines = new List<string>( );

            var lJline = ccFi[0].Trim( );

            for ( int lI = 0; lI < xTextStartPos - 1; lI++ )
            {
                if ( ccFi[lI].Trim( ).Length > 0 )
                    lJsonlines.Add( ccFi[lI] );
            }

            return lJsonlines;
        }

        /// <summary>
        /// Der Text-Part
        /// </summary>
        /// <param name="xTextStartPos"></param>
        /// <returns></returns>
        private List<string> splitTextPart( int xTextStartPos )
        {
            var lTextlines = new List<string>( );
            for ( int lI = xTextStartPos - 1; lI < ccFi.Length; lI++ )
            {
                lTextlines.Add( ccFi[lI] );
            }

            return lTextlines;
        }



    }
}
