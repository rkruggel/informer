# Wie Mitmachen

Alle Beiträge und jegliche Hilfe sind willkommen! Sie können nicht programmieren? Kein Problem! Es gibt viele Möglichkeiten, einen Beitrag zur Open-Source-Projekten zu leisten: Melden von Fehlern, Hilfe bei der Dokumentation, die Verbreitung des Wortes und natürlich, das Hinzufügen neue Features und Patches. 

## Starten

- Achten Sie darauf, eine *BitBucket* Konto haben. [https://bitbucket.org/](https://bitbucket.org/)
- Öffnen Sie eine *neues Ticket*
- Vergewissern sie sich, dass dieses Problem/Erweiterung noch nicht beschrieben worden ist.
- Beschreiben Sie das Problem. Wenn es ein Fehler ist beschreiben sie auch die Schritte wie dieser zu reproduzieren ist.


## Änderungen durchführen

- Fork das Repository auf BitBucket.
- Erstelle ein Thema branch, worauf deine Arbeit basieren soll.
- Dies ist normalerweise der `develop` branche.
- Vermeiden Sie die direkte Arbeit auf ``develop`` Branche.
- Stelle die Commits von logischen Einheiten. (falls erforderlich rebase deinen branch vor dem push).
- Prüfe unnötige Leerzeichen mit ``git diff --check``.
- Vergewissere dich, das Commit-Nachrichten im ``richtige format`` sind.
- Wenn du einen ``open issue`` bearbeitest, dann verweise in der Commit-Nachricht auf die entsprechenden Nummer (#15).
- Achte darauf, die erforderlichen Tests für die Änderungen hinzugefügt haben.
- Führen Sie alle Tests durch, um nichts versehentlich ein buggy source zu committen.
- Vergessen Sie nicht, sich selbst zu AUTOREN.md hinzuzufügen.

Diese Richtlinien gelten auch dann, wenn die Hilfe bei der Dokumentation (eigentlich für Tippfehler und kleinere Ergänzungen Sie vielleicht ``Fork und edit`` wählen).

## Submitting Changes

- commit die Änderungen in dein locales Repository.
- Führe einen ``Pull Request`` durch.
- Warte auf das feedback vom Maintainer. 


## Dein erster Beitrag?

Es ist in Ordnung. Wir haben alle mal angefangen. Siehe nächstes Kapitel.


## Du weist nicht wie du anfangen sollst? 

Es gibt mehrere Möglichkeiten mit Hilfe zu starten.

#### Du programmierst

Meistens gibt es mehrere TODO Kommentare die auf der Codebasis verstreut sind. Vielleicht suchen du sie heraus und sehen ob du eine Idee haben dieses zu lösen. 

#### offene issues

Überprüfe auch, die ``offene issues``. Hier gibt es vielleicht den Fall der dein Interesse weckt.

#### Dokumentation

Und was ist mit der Dokumentation?  
Sie wird in deutsch geschrieben. Dieses ist ein deutsches Projekt. Aber es sollte auch zu jeder Doku eine englische Version geben.

