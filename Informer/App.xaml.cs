﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Deployment.Application;
using System.IO;
using System.Linq;
using System.Runtime.Hosting;
using System.Threading.Tasks;
using System.Windows;

namespace Informer
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup( StartupEventArgs e )
        {
            //string lFname = getPath( e );

            //if ( lFname != null )
            //{
            this.Properties["ArbitraryArgName"] = getPath( e );
            //}


            base.OnStartup( e );
        }


        /// <summary>
        /// Pfad des Programmstartes holen.
        /// Beim Doppelklick auf einen File
        /// </summary>
        /// <seealso cref="https://stackoverflow.com/questions/4233915/wpf-click-once-double-click-file-to-launch-vs-2008"/>
        /// <param name="e"></param>
        /// <returns></returns>
        private static string getPath( StartupEventArgs e )
        {
            if ( !ApplicationDeployment.IsNetworkDeployed )
                return e.Args.Length != 0 ? e.Args[0] : null;
            if ( AppDomain.CurrentDomain.SetupInformation.ActivationArguments == null )
                return null;
            var args = AppDomain.CurrentDomain.SetupInformation.ActivationArguments.ActivationData;
            return args == null || args.Length == 0 ? null : new Uri( args[0] ).LocalPath;
        }


    }

}


