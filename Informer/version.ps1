﻿# Wenn scripte gesperrt sind.
# Set-ExecutionPolicy Bypass unter Powershell-ISE (als Administrator) ausführen.
# Es muss die x86 Version ausgeführt werden.
# http://www.blogperle.de/powershell-skript-ausfuehrung-deaktiviert/

# -------------------------------
# setversion.ps1  Powershell prog
#
# Roland Kruggel, 12.12.2017
#
# -------------------------------

# Aufruf:  powershell.exe "C:\Users\roland.kruggel\setver\version.ps1" "C:\Users\roland.kruggel\setver\" "Release"
# im Studio: im Präbuild: powershell.exe $(ProjectDir)version.ps1 $(ProjectDir) $(ConfigurationName)

# aufrufparameter 
param(
  [string]$version = 'C:\Users\roland.kruggel\setver\',
  [string]$debug = 'demo'
  )

# Version.txt wird als Versionsfile festgelegt
$version = Join-Path $version 'Version.txt'

# Wenn der Version.txt File nicht vorhanden ist wird er hier erstellt und mit Startwerten gefüllt
if (-not ( $version | Test-Path)) { 
    $ausgabe = "Datum:01.01.1900 Counter:0 Version:1.0.0 Compile:Demo" | Out-File -FilePath $version
}



# Input File laden
$input = Get-Content $version

$bsp = $input.Split(' ')
$bsp1 = ''

foreach ($ii in $bsp) {
    $t1 = $ii.split(':')
    if($t1[0] -eq 'Datum') {
        $bsp1 = $bsp1 + $t1[0] + ":" + (Get-Date -Format dd.MM.yyyy) + " "
    }
    if($t1[0] -eq 'Counter') {
        $bsp1 = $bsp1 + $t1[0] + ":" + ([int]$t1[1] + 1) + " "
    }
    if($t1[0] -eq 'Version') {
        $bsp1 = $bsp1 + $t1[0] + ":" + ($t1[1]) + " "
    }
    if($t1[0] -eq 'Compile') {
        $bsp1 = $bsp1 + $t1[0] + ":" + $debug
    }
    
}

#$bsp
#$bsp1

$Ausgabe = $bsp1 | Out-File -FilePath $version
 

